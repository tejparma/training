class Calculate{
	public double add(double num1, double num2){
		return num1+num2;
	}
	public double subtract(double num1, double num2){
		return num1-num2;
	}
	public double multiply(double num1, double num2){
		return num1*num2;
	}
	public double divide(double num1, double num2){
		return num1/num2;
	}
}

class Question1{
	public static void main(String[] args){
		double num1, num2;
		int choice;
		num1 = Double.parseDouble(args[0]);
		num2 = Double.parseDouble(args[1]);
		choice = Integer.parseInt(args[2]);

		double answer[] = new double[4];
		Calculate c = new Calculate();
		answer[0] = c.add(num1, num2);
		answer[1] = c.subtract(num1, num2);
		answer[2] = c.multiply(num1, num2);
		answer[3] = c.divide(num1, num2);

		System.out.println(answer[choice-1]);
	}
}