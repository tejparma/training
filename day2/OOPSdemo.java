//Inheritance
//Single level inheritance
class Person{

	public void display(){
		System.out.println("Person");
	}
	public void displayNew(){
		System.out.println("Person + Doctor");
	}
}

class Doctor extends Person{
	public void display(){
		System.out.println("Doctor");
	}
	
}

public class OOPSdemo{
	public static void main(String[] args){
		Person p = new Person();
		p.display();

		Doctor d = new Doctor();
		d.displayNew();
	}
}