package cardGame;

import java.util.concurrent.Semaphore;

class GameLogic {

	int flag = 1;
	
	public synchronized void g1() {
		if(flag == 1) {
			System.out.println("P1");
			flag = 2;
			notifyAll();
		}else {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	}
	public synchronized void g2() {
		if(flag == 2) {
			System.out.println("P2");
			flag = 3;
			notifyAll();
		}else {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	}
	public synchronized void g3() {
		if(flag == 3) {
			System.out.println("P3");
			flag = 1;
			notifyAll();
		}else {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	}
}

class Player1 extends Thread{
	GameLogic gl;
	public Player1(GameLogic gl) {
		this.gl = gl;
	}
	@Override
	public void run() {
		gl.g1();
	}

}

class Player2 extends Thread{
	GameLogic gl;
	public Player2(GameLogic gl) {
		this.gl = gl;
	}
	
	public void run() {
		
			gl.g2();
		
	}
}

class Player3 extends Thread{
	GameLogic gl;
	public Player3(GameLogic gl) {
		this.gl = gl;
	}
	
	public void run() {
		for(int i=0; i<2; i++)
			gl.g3();	
		
	}
}

public class Game{
	public static void main(String[] args) {
		GameLogic gameLogic = new GameLogic();
		Player1 p1 = new Player1(gameLogic);
		Player2 p2 = new Player2(gameLogic);
		Player3 p3 = new Player3(gameLogic);
		
		p1.start();
		p2.start();
		p3.start();
	}
}
