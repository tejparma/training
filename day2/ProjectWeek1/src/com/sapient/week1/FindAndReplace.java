package com.sapient.week1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FindAndReplace {
	
	public static void findInFile(String word) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\input.txt"));
		
		String line = br.readLine();
		while(line != null) {
			if(line.contains(word))
				System.out.println(line);
			line = br.readLine();
		}
		br.close();
	}
	
	public static void replace(String oldWord, String newWord) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\input.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\temp.txt"));
		
		String line = br.readLine();
		while(line != null) {
			if(line.contains(oldWord)) {
				line = line.replaceAll(oldWord,newWord);
			}
				bw.write(line);
				bw.newLine();
			line = br.readLine();
		}
		
		br.close();
		bw.close();

		BufferedWriter bw1 = new BufferedWriter(new FileWriter("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\input.txt"));
		BufferedReader br1 = new BufferedReader(new FileReader("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\temp.txt"));
		
		String line1 = br1.readLine();
		while(line1 != null) {
			bw1.write(line1);
			bw1.newLine();
			line1 = br1.readLine();
		}
		br1.close();
		bw1.close();
		
		if(new File("C:\\Users\\tejparma\\day2\\ProjectWeek1\\src\\com\\sapient\\week1\\temp.txt").delete())
			System.out.println("temp deleted");
		
	}

	public static void main(String[] args) throws IOException {
		
		replace("hi", "bye");
		
	}
	
}
