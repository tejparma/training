package com.sapient.week1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class IntegerArray {
	private int size;
	private int[] list;
	
	//0 argument Constructor
	public IntegerArray() {
		this.size = 10;
		list = new int[10];
	}
	
	//1 argument Constructor
	public IntegerArray(int size) {
		this.size = size;
		list = new int[size];
	}
	
	//Copy Constructor
	IntegerArray(IntegerArray ia){
		size = ia.size;
		list = ia.list;
	}
	
	//adopt the array
	public IntegerArray(int[] l) {
		list = l;
	}
	
	//Read the array
	public void read() {
		System.out.println("Enter " + list.length + "elements");
		for(int i=0; i<list.length; i++) {
			list[i] = Read.sc.nextInt();
		}
	}
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public void sortArray() {
		Arrays.sort(list);
	}
	
	public double average() {
		int sum = 0;
		for (Integer l : list) {
	        sum += l;
	    }
		return sum/size;
	}
	
	public void display() {
		for(int i=0; i<list.length; i++) {
	        System.out.print(list[i] + " ");
	    }
		System.out.println();
	}
	
	public void display(int n) {
		for(int i=0; i<list.length; i++){
	        System.out.println(list[i]);
	    }
	}
	
}
