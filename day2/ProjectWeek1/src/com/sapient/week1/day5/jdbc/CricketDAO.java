package com.sapient.week1.day5.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	public List<PlayerBean> getPlayers() throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("SELECT ID, First_Name, Last_Name, Jersey_Number FROM Player");
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> list = new ArrayList<PlayerBean>();
		PlayerBean player;
		while(rs.next()) {
			player = new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			list.add(player);
		}
		
		return list;
	}
	
	public List<PlayerBean> getAnyPlayer(int ID) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("SELECT ID, First_Name, Last_Name, Jersey_Number FROM Player WHERE ID = ?");
		ps.setInt(1, ID);
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> list = new ArrayList<PlayerBean>();
		PlayerBean player;
		while(rs.next()) {
			player = new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			list.add(player);
		}
		return list;
	}
	
	public int insertPlayer(PlayerBean player) throws Exception {
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("INSERT INTO Player VALUES (?,?,?,?)");
		ps.setInt(1, player.getID());
		ps.setString(2, player.getFirstName());
		ps.setString(3, player.getLastName());
		ps.setInt(4, player.getJerseyNumber());
		
		int flag = ps.executeUpdate();
		co.commit();
		return flag;
	}
	
	public int updatePlayer(PlayerBean player) throws Exception {
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("UPDATE Player SET Last_Name = ? WHERE ID = ?");
		ps.setString(1, player.getLastName());
		ps.setInt(2, 5);
		int flag = ps.executeUpdate();
		co.commit();
		return flag;
	}
}
