package com.sapient.week1.day5.jdbc;

import java.util.List;

public class DemoDBMS {
	public static void main(String[] args) {
		try {
			List<PlayerBean> ob = new CricketDAO().getPlayers();
			for(PlayerBean player: ob) {
				System.out.println(player);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("______________");
		CricketDAO cdao = new CricketDAO();
		try {
			cdao.updatePlayer(new PlayerBean(5,"Jaspreet", "Bumrah", 93));
			List<PlayerBean> ob = new CricketDAO().getPlayers();
			for(PlayerBean player: ob) {
				System.out.println(player);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
}
