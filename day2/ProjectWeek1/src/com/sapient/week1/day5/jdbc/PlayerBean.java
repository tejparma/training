package com.sapient.week1.day5.jdbc;

public class PlayerBean {
	private int ID;
	private String firstName;
	private String lastName;
	private int jerseyNumber;
	
	public PlayerBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PlayerBean(int iD, String firstName, String lastName, int jerseyNumber) {
		ID = iD;
		this.firstName = firstName;
		this.lastName = lastName;
		this.jerseyNumber = jerseyNumber;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getJerseyNumber() {
		return jerseyNumber;
	}

	public void setJerseyNumber(int jerseyNumber) {
		this.jerseyNumber = jerseyNumber;
	}
	
	public String toString() {
		return ID + " " + firstName + " " + lastName + " " + jerseyNumber;
	}
}
