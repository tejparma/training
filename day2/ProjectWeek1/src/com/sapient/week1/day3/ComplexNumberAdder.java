package com.sapient.week1.day3;

public class ComplexNumberAdder {
	public ComplexNumber add(ComplexNumber c1, ComplexNumber c2) {
		ComplexNumber sum = new ComplexNumber(c1.getRealPart()+c2.getRealPart(), c1.getImaginaryPart()+c2.getImaginaryPart());
		return sum;
	}
}
