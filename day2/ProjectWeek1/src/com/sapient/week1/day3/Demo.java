package com.sapient.week1.day3;

public class Demo {

	public static void main(String[] args) {
		GArray<Integer> intArray = new GArray<Integer>(new Integer[] {1,2,3,4,5});
		intArray.display();
	
		GArray<String> s = new GArray<String>(new String[] {"Tejas", "Parmar"});
		s.display();
	}

}
