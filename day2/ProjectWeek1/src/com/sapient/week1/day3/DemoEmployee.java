package com.sapient.week1.day3;

import java.util.HashMap;
import java.util.Map;


public class DemoEmployee {
	public static void main(String[] args) {
		Map<Integer, Employee> listOfEmployee = new HashMap<Integer, Employee>();
		
		Employee e1 = new Employee("Tejas", 21);
		Employee e2 = new Employee("Mayur", 25);
		Employee e3 = new Employee("Sejpal", 42);
		Employee e4 = new Employee("Shenvi", 20);
		Employee e5 = new Employee("Rohan", 22);
		Employee e6 = new Employee("Nikhil", 22);
		Employee e7 = new Employee("Heena", 41);
		
		listOfEmployee.put(101, e1);
		listOfEmployee.put(102, e2);
		listOfEmployee.put(103, e3);
		listOfEmployee.put(104, e4);
		listOfEmployee.put(105, e5);
		listOfEmployee.put(106, e6);
		listOfEmployee.put(107, e7);
		
		
	}
}
