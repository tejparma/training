package com.sapient.week1.day3;

import com.sapient.week1.Read;

public class ComplexNumberView {
	public ComplexNumber ReadComplexNumber() {
		System.out.println("Enter real and imaginary part of the complex number: ");
		return new ComplexNumber(Read.sc.nextInt(), Read.sc.nextInt());
	}
	
	public void display(ComplexNumber c) {
		System.out.println(c);
	}
}
