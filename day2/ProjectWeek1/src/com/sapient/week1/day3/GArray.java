package com.sapient.week1.day3;

public class GArray<E> {
	private Object[] arr;
	private int size;
	
	public GArray() {
		size = 10;
		arr = new Object[size];
	}
	
	public GArray(int size) {
		this.size = size;
		arr = new Object[size];
	}
	
	public GArray(E[] e) {
		arr = e;
		size = e.length;
	}
	
	public GArray(GArray<E> g) {
		arr = g.arr;
		size = g.size;
	}
	
	public void display() {
		for (Object object : arr) {
			System.out.println(object);
		}
	}
	
}
