package com.sapient.week1.day3;

public class DemoCN {
	public static void main(String[] args) {
		ComplexNumberView view = new ComplexNumberView();
		ComplexNumberAdder model = new ComplexNumberAdder();
		
		ComplexNumber cn1 = view.ReadComplexNumber();
		ComplexNumber cn2 = view.ReadComplexNumber();
		
		view.display(model.add(cn1, cn2));
	}
}
