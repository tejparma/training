package com.sapient.week1.day3;

public class ComplexNumber {
	private int realPart;
	private int imaginaryPart;
	
	public ComplexNumber(int x, int y) {
		this.realPart = x;
		this.imaginaryPart = y;
	}
	
	public int getRealPart() {
		return realPart;
	}
	public void setRealPart(int realPart) {
		this.realPart = realPart;
	}
	public int getImaginaryPart() {
		return imaginaryPart;
	}
	public void setImaginaryPart(int imaginaryPart) {
		this.imaginaryPart = imaginaryPart;
	}
	
	@Override
	public String toString() {
		return realPart + " + " + imaginaryPart + "i";
	}
	
}
