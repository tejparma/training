package com.sapient.week1.day3.mvc;

public class Demo {
	public static void main(String[] args) {
		AddBean addBean = new AddBean();
		View view = new View();
		AddDAO addDao = new AddDAO();
		
		view.read(addBean);
		addDao.compute(addBean);
		view.display(addBean);
		
		view = null;
		addBean = null;
		addDao = null;
		
		System.gc();

	}
	
}
