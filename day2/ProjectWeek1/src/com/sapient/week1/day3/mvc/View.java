package com.sapient.week1.day3.mvc;

import com.sapient.week1.Read;

public class View {
	public void read(AddBean ob) {
		System.out.println("Enter two numbers");
		ob.setNum1(Read.sc.nextInt());
		ob.setNum2(Read.sc.nextInt());
	}
	
	public void display(AddBean ob) {
		System.out.println(ob.getNum1());
		System.out.println(ob.getNum2());
		System.out.println(ob.getNum3());
	}
}
