package com.sapient.week1.day3.mvc;

public class AddDAO {
	public void compute(AddBean ob) {
		ob.setNum3(ob.getNum1() + ob.getNum2());
	}
}
