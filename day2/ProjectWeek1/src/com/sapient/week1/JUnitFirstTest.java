package com.sapient.week1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JUnitFirstTest {

	private JUnitFirst f;
	
	@Test
	void test1() {
		f = new JUnitFirst();
		assertEquals(true, f.isPrime(-2));
	}
	
	@Test
	void test2() {
		f = new JUnitFirst();
		assertEquals(true, f.isPrime(7));
	}
	
	@Test
	void test3() {
		f = new JUnitFirst();
		assertEquals(false, f.isPrime(9));
	}
	
	@Test
	void test() {
		f = new JUnitFirst();
		assertEquals(false, f.isPrime(4));
	}

}
