package com.sapient.week1;

public class EmployeeMain {
	
	public static void main(String[] args) {
	
		EmployeeData ed = new EmployeeData();
		ed.insertData(new Employee(1,"Tejas", 21));
		ed.insertData(new Employee(2, "Shenvi", 20));
		ed.insertData(new Employee(3, "Viz", 22));
		
		for(Employee e : ed.getList(0)) {
			System.out.println(e);
		}
		
		for(Employee e : ed.getList(1)) {
			System.out.println(e);
		}
	}

}
