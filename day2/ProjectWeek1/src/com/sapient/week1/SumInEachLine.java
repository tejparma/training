package com.sapient.week1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SumInEachLine {

	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\test.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\sum.txt"));
		
		String line = br.readLine();
		while(line != null) {
			String[] nums = line.split(",");
			int sum = 0;
			for(String s : nums) {
				sum += Integer.parseInt(s);
			}
			bw.write(String.valueOf(sum));
			bw.newLine();
			line = br.readLine();
		}
		br.close();
		bw.close();
	}
	
}
