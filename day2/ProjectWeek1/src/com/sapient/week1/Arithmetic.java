package com.sapient.week1;

public class Arithmetic {
	//two methods cannot have same signature
	//Signature:
		//name
		//number of argument
		//type of arguments
		//order of arguements
		//*return type is not a part of signature*

	public int add(int a, int b) {
		return a+b;
	}
	
	public float add(float a, int  b) {
		return a+b;
	}
	
}
