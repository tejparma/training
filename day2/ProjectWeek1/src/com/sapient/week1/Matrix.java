package com.sapient.week1;

public class Matrix {
	private int M;
	private int N;
	
	int[][] matrix;
	
	//Default Constructor
	public Matrix() {
		M = 3;
		N = 3;
		matrix = new int[M][N];
	}
	
	//Constructor with size
	public Matrix(int M, int N) {
		this.M = M;
		this.N = N;
		matrix = new int[M][N];
	}
	
	//Adopt a matrix
	public Matrix(int[][] data) {
		M = data.length;
		N = data[0].length;
		matrix = new int[M][N];
		for(int i=0; i<M; i++) {
			for(int j=0; j<N; j++) {
				matrix[i][j] = data[i][j];
			}
		}
	}
	
	//Copy Constructor
	public Matrix(Matrix m) {
		this.matrix = m.matrix;
	}
	
	//Display the matrix
	public void printMatrix() {
		for(int i=0; i<M; i++) {
			for(int j=0; j<N; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	//Read the matrix from the user
	public void read() {
		System.out.println("Enter matrix of size " + matrix.length + ", " + matrix[0].length);
		for(int i=0; i<M; i++) {
			for(int j=0; j<N; j++) {
				matrix[i][j] = Read.sc.nextInt();
			}
		}
	}
	
	//Add two matrices
	public Matrix add(Matrix ob) {
		Matrix temp = new Matrix(this.matrix.length, this.matrix[0].length);
		for(int i=0; i<M; i++) {
			for(int j=0; j<N; j++) {
				temp.matrix[i][j] = this.matrix[i][j] + ob.matrix[i][j];
			}
		}
		
		return temp;
	}
	
	//Multiply two matrices
	public Matrix multiply(Matrix ob) {
		
		Matrix temp;
		if(this.matrix[0].length != ob.matrix.length) {
			return (Matrix)new Object();
		}
		temp = new Matrix(this.matrix.length, ob.matrix[0].length);
		
		for(int i=0; i<this.M; i++) {
			for(int j=0; j<this.N; j++) {
				temp.matrix[i][j] = 0;
				for(int k=0; k< ob.N; k++ ) {
					temp.matrix[i][j] += this.matrix[i][k]*ob.matrix[k][j];
				}
			}
		}
		
		return temp;
	}
	
}
