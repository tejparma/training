package com.sapient.week1;

public class Student {
	//LOWEST TO HIGHEST VISIBILITY
	private int a = 10;
	//visible in the same package
	int b = 40;
	//protected- visible in the same package but available outside package through inheritance
	protected int c = 20;
	//available anywhere - if member has to be visible outside the package
	public int d = 30;
}
