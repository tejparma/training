package com.sapient.week1.ticktock;

class Game{
	public synchronized void g1() {
		notify();
		System.out.println("Tick");
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void g2() {
		notify();
		System.out.println("Tock");
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class Player1 extends Thread{
	Game ob;
	public Player1(Game ob) {
		this.ob= ob;
	}
	
	public void run() {
		for(int i=0; i<10; i++) {
			ob.g1();
		}
	}
}

class Player2 extends Thread{
	Game ob;
	public Player2(Game ob) {
		this.ob= ob;
	}
	
	public void run() {
		for(int i=0; i<10; i++) {
			ob.g2();
			if(i==0) {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}

public class Demo {
	public static void main(String[] args) {
		Game game = new Game();
		Player1 p1 = new Player1(game);
		Player2 p2 = new Player2(game);
		p1.start();
		p2.start();
	}
		
}
