package com.sapient.week1.ticktock;

public class Player implements Runnable{
	private Thread currentThread;
	private static final int N = 3;
	private String value;
	private static Object obj = new Object();
	
	public Player(String value) {
		this.value = value;
		currentThread = new Thread(this);
		currentThread.start();
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public void run() {
		for(int counter=0; counter<N; counter++) {
			synchronized (obj) {
				System.out.println(this.getValue());
				obj.notify();
				//System.out.println("___________________");
				try {
					obj.wait();
					//System.out.println("+++++++++++++++++");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			}
		}
	}
}
