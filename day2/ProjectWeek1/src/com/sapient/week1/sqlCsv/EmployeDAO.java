package com.sapient.week1.sqlCsv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeDAO {
	
	List<EmployeeBean> list = new ArrayList<EmployeeBean>();
	BufferedReader br;
	public void displayData(String query) {
		
	}
	
	public void displayAll() {
		list.stream().forEach(System.out::println);
	}
	
	public void saveData(File file) throws IOException {
		
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line = br.readLine();
		while(line!=null) {
			String[] temp = line.split(",");
			EmployeeBean empB = new EmployeeBean(temp[0], Integer.parseInt(temp[1]), temp[2], Double.parseDouble(temp[3]));
			list.add(empB);
			line = br.readLine();
		}
		
	}
	
}
