package com.sapient.week1;

public class Demo3 {
	public static void main(String[] args) {
		Matrix A = new Matrix();
		A.read();
		
		Matrix B = new Matrix(new int[][] {{1,1,1},{1,1,1},{1,1,1}});
		Matrix C = A.add(B);
		Matrix D = A.multiply(B);
		D.printMatrix();
	}
}
