package com.sapient.week1;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * 
 * SHOULD BE NAMED EmployeeDAO
 * 
 */


public class EmployeeData {
	 
	Map<Integer, Employee> map = new HashMap<Integer, Employee>();
	public void insertData(Employee e) {
		map.put(e.getId(), e);
	}
	
	public Employee getData(Integer key) {
		return map.get(key);
	}
	
	public List<Employee> getList(int i){
		List<Employee> l1 = map.values().stream().collect(Collectors.toList());
		if(i==0) 
			Collections.sort(l1, (o, o1) -> o.getName().compareTo(o1.getName()));
		else
			Collections.sort(l1, (o, o1) -> o.getAge()-o1.getAge());
		return l1;
	}
	
}
