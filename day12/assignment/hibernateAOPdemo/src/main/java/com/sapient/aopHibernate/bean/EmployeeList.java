package com.sapient.aopHibernate.bean;

import java.util.ArrayList;
import java.util.List;

public class EmployeeList {
	List<EmployeeBean> list = new ArrayList<EmployeeBean>();

	public EmployeeList(List<EmployeeBean> list) {
		super();
		this.list = list;
	}

	public EmployeeList() {
		super();
	}

	public List<EmployeeBean> getList() {
		return list;
	}

	public void setList(List<EmployeeBean> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "EmployeeList [list=" + list + "]";
	}

}
