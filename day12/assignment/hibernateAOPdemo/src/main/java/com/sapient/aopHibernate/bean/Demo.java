package com.sapient.aopHibernate.bean;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);

		EmployeeList emplist = context.getBean(EmployeeList.class);

		ArrayList<EmployeeBean> list = (ArrayList<EmployeeBean>) emplist.getList();

		System.out.println(list);

	}
}
