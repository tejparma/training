package com.sapient.spring.demos;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {

	@Bean
	@Scope("singleton")
	public Hello get1() {
		return new Hello();
	}

	@Bean
	public Employee get2() {
		return new Employee("Tejas Parmar", 21);
	}

	@Bean
	public Employee get3() {
		return new Employee("Sejpal Parmar", 44);
	}

	@Bean
	public HolidayList get4(){
		HolidayList ob = new HolidayList();
		ob.getList().add(new Holiday("Independence Day", "15th August"));
		ob.getList().add(new Holiday("Republic Day", "26th January"));
		return ob;
	}

}
