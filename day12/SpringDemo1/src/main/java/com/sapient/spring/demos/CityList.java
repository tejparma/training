package com.sapient.spring.demos;

import java.util.ArrayList;

public class CityList {
	ArrayList<String> list;

	public CityList() {
		super();

	}

	public CityList(ArrayList<String> list) {
		super();
		this.list = list;
	}

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "CityList [list=" + list + "]";
	}
	
}
