package com.sapient.spring.demos;

public class Holiday {
	private String holidayName;
	private String holidayDate;
	public Holiday() {
		super();
	}
	public Holiday(String holidayName, String holidayDate) {
		super();
		this.holidayName = holidayName;
		this.holidayDate = holidayDate;
	}
	public String getHolidayName() {
		return holidayName;
	}
	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}
	public String getHolidayDate() {
		return holidayDate;
	}
	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}
	@Override
	public String toString() {
		return "Holiday [holidayName=" + holidayName + ", holidayDate=" + holidayDate + "]";
	}
}	
