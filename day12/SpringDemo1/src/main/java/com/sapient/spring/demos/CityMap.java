package com.sapient.spring.demos;

import java.util.HashMap;

public class CityMap {
	HashMap<String, CityList> map;

	public CityMap() {
		super();

	}

	public CityMap(HashMap<String, CityList> map) {
		super();
		this.map = map;
	}

	public HashMap<String, CityList> getMap() {
		return map;
	}

	public void setMap(HashMap<String, CityList> map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return "CityMap [map=" + map + "]";
	}
	
	
}
