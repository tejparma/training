package com.sapient.spring.demos;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo2 {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
		Hello ob;
		ob = context.getBean(Hello.class);
		ob.display();
		Employee emp;
		emp = context.getBean(Employee.class);
		System.out.println(emp.toString());
		HolidayList hl;
		hl = context.getBean(HolidayList.class);
		System.out.println(hl);
	}
}
