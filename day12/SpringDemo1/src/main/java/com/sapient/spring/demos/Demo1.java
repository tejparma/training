package com.sapient.spring.demos;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		HolidaySet ob;
		ob = (HolidaySet) context.getBean("set");
		CityMap cm;
		cm = (CityMap) context.getBean("map");
		System.err.println(cm);
		Employee emp;
		emp = (Employee) context.getBean("emp");
		System.err.println(emp);
	}
}
