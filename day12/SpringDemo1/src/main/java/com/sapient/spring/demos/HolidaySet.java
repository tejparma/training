package com.sapient.spring.demos;

import java.util.Set;

public class HolidaySet {
	private Set<Holiday> set;

	public HolidaySet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HolidaySet(Set<Holiday> set) {
		super();
		this.set = set;
	}

	public Set<Holiday> getSet() {
		return set;
	}

	public void setSet(Set<Holiday> set) {
		this.set = set;
	}

	@Override
	public String toString() {
		return "HolidaySet [set=" + set + "]";
	}
	
}
