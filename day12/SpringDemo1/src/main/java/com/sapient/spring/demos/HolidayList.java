package com.sapient.spring.demos;

import java.util.ArrayList;

public class HolidayList {
	private ArrayList<Holiday> list;

	public HolidayList(ArrayList<Holiday> list) {
		super();
		this.list = list;
	}

	public HolidayList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Holiday> getList() {
		return list;
	}

	public void setList(ArrayList<Holiday> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "HolidayList [list=" + list + "]";
	}
	
}
