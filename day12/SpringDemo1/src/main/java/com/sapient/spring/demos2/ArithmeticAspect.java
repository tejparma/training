package com.sapient.spring.demos2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {

	@Before("execution(* *.*(double, double))")
	public void check1(JoinPoint jpoint) {
		for (Object x : jpoint.getArgs()) {
			double v = (Double) x;
			if (v < 0) {
				throw new IllegalArgumentException("Number cannot be negative");
			}
		}
	}

	@AfterReturning(pointcut = "execution(* *.*(double, double))", returning = "retVal")
	public void check2(JoinPoint jpoint, Object retVal) {
		double v = (Double) retVal;
		if (v < 0) {
			throw new IllegalArgumentException("Result should not be negative");
		}
	}

	@Around("execution(* *.*(double, double))")
	public double check3(ProceedingJoinPoint pjpoint) throws Throwable {
		Object[] args = pjpoint.getArgs();
		Object result = null;
		for (int i = 0; i < args.length; i++) {
			if ((Double) args[i] < 0) {
				throw new IllegalArgumentException("Numbers cannot be negative");
			}
		}
		result = pjpoint.proceed(args);
		if ((Double) result < 0)
			throw new IllegalArgumentException("Result should not be negative");

		return (Double) result;
	}
}
