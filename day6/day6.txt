MAVEN:

0. mkdir
1. mvn archetype:generate
2. select archetype
3. select version
4. set groupID
5. set artifact ID

6. mvn compile
7. mvn package
8. java -cp "Path" com.sapient.week2.maven.App 

Maven has a build life cycle
it consists of phases
compile,  test, etc are phases
maven automatically calls previous phases if not mentioned.
for example, if package is called then compile is automatically called
phases:
	validate
	compile
	test
	package
	install (its not on server..it is local)
	deploy (its not on server..it is local)


DOUBTS:
hashCode
JUnit testing
