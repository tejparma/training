package com.sapient.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDetailsDAO sDetails;
	@Autowired
	StudentMarksDAO sMarks;

	@PostMapping("/addStudent")
	public String storeStudentDetails(@RequestBody StudentDetails sd) {
		sDetails.save(sd);
		return "Saved";
	}
	
	@PostMapping("/addMarks")
	public String storeStudentMarks(@RequestBody StudentMarks sm) {
		sMarks.save(sm);
		return "Saved";
	}
	
	@GetMapping("/displayS")
	public List<StudentDetails> display(){
		return (List<StudentDetails>) sDetails.findAll();
	}
	
	@GetMapping("/displayM")
	public List<StudentMarks> displayM(){
		return (List<StudentMarks>) sMarks.findAll();
	}
	
	@GetMapping("/displayS/{id}")
	public Optional<StudentDetails> displayStudentById(@PathVariable String id) {
		return sDetails.findById(id);
	}
	
}
