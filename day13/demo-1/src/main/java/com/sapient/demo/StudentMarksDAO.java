package com.sapient.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface StudentMarksDAO extends CrudRepository<StudentMarks, String>{

}
