package com.sapient.demo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class StudentDetails {
	@Id
	private String regNo;
	@Column
	private String fname;
	@Column
	private String lname;
	@Column
	private String dob;
	@Column
	private String cityCode;
	@Transient

	private Set<StudentMarks> sm = new HashSet<>();;
	
	public StudentDetails() {
		super();
	}
	public StudentDetails(String regNo, String fname, String lname, String dob, String cityCode) {
		super();
		this.regNo = regNo;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.cityCode = cityCode;
	}
	public String getId() {
		return regNo;
	}
	public void setId(String regNo) {
		this.regNo = regNo;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	/*
	 * mapping regNo in SD class to SM marks 
	 */
	
	@OneToMany(mappedBy = "regNo")
	public Set<StudentMarks> getSm() {
		return sm;
	}
	public void setSm(Set<StudentMarks> sm) {
		this.sm = sm;
	}
	
}
