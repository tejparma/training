package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
	
	@GetMapping("/hello")
	public String display() {
		return "Welcome to Spring Boot Microservices";
	}
	
	@GetMapping("/hello1")
	public Student display1() {
		return new Student("Tejas", 21, "Mumbai");
	}

	@GetMapping("/students")
	public List<Student> getStudents(){
		return ob.getStudents();
	}
	
	@GetMapping("/student/{name}")
	public List<Student> getStudents3(@PathVariable String name){
		return ob.getDetails(name);
	}
	
	@DeleteMapping("/student/{name}")
	public List<Student> deleteStudent(@PathVariable String name){
		return ob.deleteStudent(name);
	}
	
	@PostMapping("/student")
	public List<Student> createStudent(@RequestBody Student stud){
		return ob.createStudent(stud);
	}
}
