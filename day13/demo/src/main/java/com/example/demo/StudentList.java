package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
	private List<Student> list;

	public StudentList() {
		super();
		list = new ArrayList<>();
		list.add(new Student("Tejas", 21, "Mumbai"));
		list.add(new Student("Shenvi", 20, "Surat"));
		list.add(new Student("Mayur", 25, "Bahrain"));
		list.add(new Student("Dhaval", 31, "London"));
		list.add(new Student("Shwet", 30, "Mumbai"));
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "StudentList [list=" + list + "]";
	}
	
	
}
