package com.example.demo;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {

	@Autowired
	StudentList l1;

	public List<Student> getStudents() {
		return l1.getList();
	}

	public List<Student> getDetails(String name) {
		return l1.getList().stream().filter(student -> student.getName().equals(name)).collect(Collectors.toList());
	}

	
	public List<Student> deleteStudent(String name) {
		Iterator<Student> itr = l1.getList().iterator();
		while (itr.hasNext()) {
			Student stud = itr.next();

			if (stud.getName().equals(name)) {
				itr.remove();
			}

		}
		return l1.getList();
	}
	
	public List<Student> createStudent(Student stud){
		l1.getList().add(stud);
		return l1.getList();
	}
}
