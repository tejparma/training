package com.sapient.demo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class Test4 {
	static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student/Siddhant";
	
	public static void main(String[] args) {

		Student newStud = new Student();
		System.out.println("Enter name, age and city");

		newStud.setName(Read.sc.next());
		newStud.setAge(Read.sc.next());
		newStud.setCity(Read.sc.next());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);

		RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<Student> requestBody = new HttpEntity<>(newStud, headers);

		// Send request with POST method.
		restTemplate.put(URL_CREATE_EMPLOYEE, requestBody);

		System.out.println("Employee updated: ");
	}
}
