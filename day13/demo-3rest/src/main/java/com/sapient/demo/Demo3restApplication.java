package com.sapient.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3restApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demo3restApplication.class, args);
	}

}
