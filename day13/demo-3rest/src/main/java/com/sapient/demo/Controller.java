package com.sapient.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	DAO dao;
	
	@GetMapping("/service1")
	public String getService1() {
		return dao.restTemp.getForObject(dao.BASE_URL + "/hello", String.class);
	}
}
