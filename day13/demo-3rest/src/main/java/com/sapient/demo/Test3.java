package com.sapient.demo;

import java.util.Arrays;
import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

class Read {
	public static Scanner sc = new Scanner(System.in);
}

public class Test3 {

	static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student";

	public static void main(String[] args) {

		Student newStud = new Student();
		System.out.println("Enter name, age and city");

		newStud.setName(Read.sc.next());
		newStud.setAge(Read.sc.next());
		newStud.setCity(Read.sc.next());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);

		RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<Student> requestBody = new HttpEntity<>(newStud, headers);

		// Send request with POST method.
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);

		System.out.println("Employee created: ");

	}
}
