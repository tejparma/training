package com.sapient.demo;

import java.util.List;
import java.util.Optional;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	ProfileDAO pdao;
	
	@GetMapping("/display")
	public List<Profile> displayAll(){
		return (List<Profile>) pdao.findAll();
	}
	
	@GetMapping("/display/{id}")
	public Optional<Profile> displayById(@PathVariable String id){
		return pdao.findById(id);
	}
	
	@PostMapping("/profile")
	public String createProfile(@RequestBody Profile profile) {
		pdao.save(profile);
		return "Profile Created";
	}
}
