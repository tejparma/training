package com.sapient.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface ProfileDAO extends CrudRepository<Profile, String>{
	
}
