package com.sapient.demo;

public class Profile {
	private String id;
	private String name;
	private String age;
	private String city;
	public Profile() {
		super();
	}
	public Profile(String id, String name, String age, String city) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.city = city;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "Profile [id=" + id + ", name=" + name + ", age=" + age + ", city=" + city + "]";
	}
	
}
