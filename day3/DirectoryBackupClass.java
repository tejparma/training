import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DirectoryBackupClass {
	public static void main(String[] args) {
		String source, destination;
		System.out.println("Enter the source location");
		source = "R:\\AVLtrees";
		System.out.println("Enter the destinatin location");
		destination = "E:\\AVLtrees";
		
		File sourceFolder = new File(source);
		File destinationFolder = new File(destination);
		
		try {
			copyDirectory(sourceFolder, destinationFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void copyDirectory(File sourceFolder, File destinationFolder) throws IOException {
		
		
		//If the sourceFolder is directory
		if(sourceFolder.isDirectory()) {
			if(!destinationFolder.exists()) {
				destinationFolder.mkdir();
			}
			
			String[] listOfFiles = sourceFolder.list();	
			
			for (String f : listOfFiles) {
				File src = new File(sourceFolder, f);
				File dest = new File(destinationFolder, f);
				//Recursion to handle the case of folder within folder
				copyDirectory(src, dest);
			}
			
		}
		//If the sourceFolder is not a directory but file
		else {
			InputStream inputStream = new FileInputStream(sourceFolder);
			OutputStream outputStream = new FileOutputStream(destinationFolder);
			
			byte[] buffer = new byte[1024];
    	    
	        int length;
	        //copy the file content in bytes 
	        while ((length = inputStream.read(buffer)) > 0){
	    	   outputStream.write(buffer, 0, length);
	        }

	        inputStream.close();
	        outputStream.close();
		}
		
	}
}
